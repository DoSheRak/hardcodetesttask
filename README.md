# TOO Hard Code Technical Task

## Requirements

Создать ASP.NET Core Web API (net core 3.1+) приложение, в котором будут реализованы следующие страницы:

1. Категории товара
2. Создание товара
3. Просмотр списка товаров
4. Просмотр товара

##### 1. Страница категории товара

На странице категорий товара должен выводиться список категорий, с возможностью добавлять новую или удалять существующую категорию. При добавлении категории нужно добавить возможность добавлять дополнительные поля для товара этой категории, например, цвет, вес, размер и т.д.

##### 2. Страница создания товара

На странице создания товара должна быть возможность заполнить общую информацию о товаре (фотография, название, описание, цена, категория) и указать значения для дополнительных полей, которые были созданы для выбранной категории.

##### 3. Страница просмотра списка товара

Вывести список товаров с фильтром по категориям и дополнительным полям.

##### 4. Просмотр выбранного товара из списка товаров

Вывести информацию о товаре и его параметрах.

Обязательная часть включает в себя создание всего двух эндпоинтов, реализующих сохранение и получение товаров. Основной целью является проверка подхода к проектированию механизма "динамических полей", которые создаются в категории, и должны заполняться и возвращаться в товаре.

Результат выложить в GitLab, и предоставить ссылку на репозиторий для оценки реализации. В репозитории должна быть основная ветка (например, main), а каждая подзадача должна выполняться в отдельной ветке, по выполнению которой ветка подзадачи должна быть слита в основную путем создания merge request'а.

При оценке выполнения тестового задания будет оцениваться:

- Умение декомпозиции задач.
- Умение оценки задач.
- Подход к разработке задачи (проектирование).
- Знание и умение применять на практике возможности ASP.NET Core, EF и т.д.

## Start

1. Clone a project
2. Create a database
3. Change the connection string
4. dotnet run

## About the project

I have divided the project into 4 parts: Data Access Layer(DAL), Domain, Service and web API.

At first I used 3 models, but later I realized that it was better to use 4 models: Category, Product, Category Field, ProductField.

On the category page there are endpoints for creating, deleting, viewing the list, viewing a separate category and added the ability to change the category.

2, 3 and 4 tasks are all endpoints for Product. But I added more updates and product deletions.

To filter by categories and additional fields, I created the GetFilteredProducts endpoint in the ProductController.

## Last words before the court

When I was doing 1 task, I immediately implemented CRUD for categories and products. Therefore, it remained to do filtering, change the models and the relationships between them, and change the methods in the services for the new models. The test task can be completed in about 2-3 days, but this week the circumstances at the university take a lot of time. I don't want to say that this is an excuse, it's not like this every week at university. Also, this is not an ideal solution, you can add exception handling, tests, etc.