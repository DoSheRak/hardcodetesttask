﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;
using HardCodeOnlineShop.Domain.Entity;

namespace HardCodeOnlineShop.Domain.ViewModels
{
	public class CategoryViewModel
	{
        public int Id { get; set; }

        [Required(ErrorMessage = "Specify the category name")]
        [Display(Name = "Category Name")]
        public string? Name { get; set; }

        public List<string>? Fields { get; set; }
    }
}

