﻿using System;
namespace HardCodeOnlineShop.Domain.ViewModels
{
	public class ProductFieldViewModel
	{
        public int CategoryFieldId { get; set; }

        public string? Value { get; set; }
    }
}

