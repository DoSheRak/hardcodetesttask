﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;
using HardCodeOnlineShop.Domain.Entity;

namespace HardCodeOnlineShop.Domain.ViewModels
{
	public class ProductViewModel
	{
        public int Id { get; set; }

        [Required(ErrorMessage = "Specify the product name")]
        [Display(Name = "Product Name")]
        public string? Name { get; set; }

        [Display(Name = "Description")]
        public string? Description { get; set; }

        [Required(ErrorMessage = "Specify the price")]
        [Display(Name = "Price")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Select a category")]
        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        public List<ProductFieldViewModel>? Fields { get; set; }
    }
}

