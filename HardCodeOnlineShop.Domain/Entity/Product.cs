﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HardCodeOnlineShop.Domain.Entity
{
	public class Product : BaseEntity
	{
        public string? Description { get; set; }

        [Required]
        public decimal Price { get; set; }

        [ForeignKey("CategoryId")]
        public int CategoryId { get; set; }

        public Category? Category { get; set; }

        public List<ProductField>? Fields { get; set; }
    }
}

