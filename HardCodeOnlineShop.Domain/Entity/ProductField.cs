﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HardCodeOnlineShop.Domain.Entity
{
	public class ProductField
	{
        public int Id { get; set; }

        public int ProductId { get; set; }

        public int CategoryFieldId { get; set; }

        public string? Value { get; set; }
    }
}

