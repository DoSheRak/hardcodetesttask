﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace HardCodeOnlineShop.Domain.Entity
{
	public class CategoryField : BaseEntity
	{
        [Required]
        public int CategoryId { get; set; }

        [JsonIgnore]
        public Category? Category { get; set; }
    }
}

