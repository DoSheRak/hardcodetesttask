﻿using System;
namespace HardCodeOnlineShop.Domain.Entity
{
	public class FilterParam
	{
        public string? Field { get; set; }

        public int CategoryFieldId { get; set; }

        public string? Value { get; set; }
    }
}

