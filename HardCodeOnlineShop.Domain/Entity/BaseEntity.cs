﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HardCodeOnlineShop.Domain.Entity
{
	public class BaseEntity
	{
		[Key]
		public int Id { get; set; }

        [Required]
        public string? Name { get; set; }
    }
}

