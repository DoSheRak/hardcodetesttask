﻿using System;

namespace HardCodeOnlineShop.Domain.Entity
{
	public class Category : BaseEntity
	{
        public List<CategoryField>? Fields { get; set; }
    }
}

