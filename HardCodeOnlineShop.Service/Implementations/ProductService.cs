﻿using System;
using HardCodeOnlineShop.DAL.Interfaces;
using HardCodeOnlineShop.DAL.Repositories;
using HardCodeOnlineShop.Domain.Entity;
using HardCodeOnlineShop.Domain.ViewModels;
using HardCodeOnlineShop.Service.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace HardCodeOnlineShop.Service.Implementations
{
	public class ProductService : IProductService
	{
		private readonly IBaseRepository<Product> _productRepository;

		public ProductService(IBaseRepository<Product> productRepository)
		{
			_productRepository = productRepository;
		}

        public async Task<Product> CreateProduct(ProductViewModel model)
        {
            var product = new Product
            {
                Name = model.Name,
                Description = model.Description,
                Price = model.Price,
                CategoryId = model.CategoryId,
                Fields = model.Fields.Select(field => new ProductField
                {
                    CategoryFieldId = field.CategoryFieldId,
                    Value = field.Value
                }).ToList()
            };

            await _productRepository.Create(product);

            return product;
        }

        public async Task<bool> DeleteProduct(int id)
        {
            var product = await _productRepository.GetAll().FirstOrDefaultAsync(p => p.Id == id);

            if (product == null)
            {
                return false;
            }

            await _productRepository.Delete(product);

            return true;
        }

        public async Task<Product> GetProduct(int id)
        {
            return await _productRepository.GetAll()
                .Include(p => p.Fields)
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<List<Product>> GetProducts()
        {
            return await _productRepository.GetAll()
                    .Include(p => p.Fields)
                    .ToListAsync();
        }

        public async Task<Product> UpdateProduct(int id, ProductViewModel model)
        {
            var product = await _productRepository.GetAll()
                .Include(p => p.Fields)
                .FirstOrDefaultAsync(p => p.Id == id);

            if (product == null)
            {
                return null;
            }

            product.Name = model.Name;
            product.Description = model.Description;
            product.Price = model.Price;
            product.CategoryId = model.CategoryId;

            product.Fields.Clear();
            product.Fields.AddRange(model.Fields.Select(field => new ProductField
            {
                CategoryFieldId = field.CategoryFieldId,
                Value = field.Value
            }));

            await _productRepository.Update(product);

            return product;
        }
    }
}

