﻿using System;
using HardCodeOnlineShop.DAL.Interfaces;
using HardCodeOnlineShop.DAL.Repositories;
using HardCodeOnlineShop.Domain.Entity;
using HardCodeOnlineShop.Domain.ViewModels;
using HardCodeOnlineShop.Service.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace HardCodeOnlineShop.Service.Implementations
{
	public class CategoryService : ICategoryService
	{
		private readonly IBaseRepository<Category> _categoryRepository;

		public CategoryService(IBaseRepository<Category> categoryRepository)
		{
			_categoryRepository = categoryRepository;
		}

        public async Task<Category> CreateCategory(CategoryViewModel model)
        {
            var existingCategory = await _categoryRepository.GetAll()
                .FirstOrDefaultAsync(c => c.Id == model.Id);

            if (existingCategory != null)
            {
                return existingCategory;
            }

            var category = new Category
            {
                Name = model.Name,
                Fields = model.Fields.Select(fieldName => new CategoryField { Name = fieldName }).ToList()
            };

            await _categoryRepository.Create(category);

            return category;
        }

        public async Task<bool> DeleteCategory(int id)
        {
            var category = await _categoryRepository.GetAll()
                .FirstOrDefaultAsync(c => c.Id == id);

            if (category == null)
            {
                return false;
            }

            await _categoryRepository.Delete(category);

            return true;
        }

        public async Task<List<Category>> GetCategories()
        {
            return await _categoryRepository.GetAll()
                .Include(c => c.Fields)
                .ToListAsync();
        }

        public async Task<Category> GetCategory(int id)
        {
            return await _categoryRepository.GetAll()
                .Include(c => c.Fields)
                .FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<Category> UpdateCategory(int id, CategoryViewModel model)
        {
            var category = await _categoryRepository.GetAll()
                .Include(c => c.Fields)
                .FirstOrDefaultAsync(c => c.Id == id);

            if (category == null)
            {
                return null;
            }

            category.Name = model.Name;

            category.Fields.Clear();
            category.Fields.AddRange(model.Fields.Select(fieldName => new CategoryField { Name = fieldName }));

            await _categoryRepository.Update(category);
            return category;
        }
    }
}

