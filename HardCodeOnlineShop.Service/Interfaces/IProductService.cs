﻿using System;
using HardCodeOnlineShop.Domain.Entity;
using HardCodeOnlineShop.Domain.ViewModels;

namespace HardCodeOnlineShop.Service.Interfaces
{
	public interface IProductService
	{
        Task<List<Product>> GetProducts();

        Task<Product> GetProduct(int id);

        Task<Product> CreateProduct(ProductViewModel model);

        Task<bool> DeleteProduct(int id);

        Task<Product> UpdateProduct(int id, ProductViewModel model);
    }
}

