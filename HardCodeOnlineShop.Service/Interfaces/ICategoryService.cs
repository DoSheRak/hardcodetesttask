﻿using System;
using HardCodeOnlineShop.Domain.Entity;
using HardCodeOnlineShop.Domain.ViewModels;

namespace HardCodeOnlineShop.Service.Interfaces
{
	public interface ICategoryService
	{
        Task<List<Category>> GetCategories();

        Task<Category> GetCategory(int id);

        Task<Category> CreateCategory(CategoryViewModel model);

        Task<bool> DeleteCategory(int id);

        Task<Category> UpdateCategory(int id, CategoryViewModel model);
    }
}

