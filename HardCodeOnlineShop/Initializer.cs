﻿using System;
using HardCodeOnlineShop.DAL.Interfaces;
using HardCodeOnlineShop.DAL.Repositories;
using HardCodeOnlineShop.Domain.Entity;
using HardCodeOnlineShop.Service.Implementations;
using HardCodeOnlineShop.Service.Interfaces;

namespace HardCodeOnlineShop
{
	public static class Initializer
	{
		public static void InitializeRepositories(this IServiceCollection services)
		{
            services.AddScoped<IBaseRepository<Category>, CategoryRepository>();
            services.AddScoped<IBaseRepository<Product>, ProductRepository>();
		}

		public static void InitializeServices(this IServiceCollection services)
		{
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IProductService, ProductService>();
		}
    }
}

