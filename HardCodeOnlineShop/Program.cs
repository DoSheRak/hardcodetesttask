﻿using HardCodeOnlineShop;
using HardCodeOnlineShop.DAL;
using HardCodeOnlineShop.DAL.Interfaces;
using HardCodeOnlineShop.DAL.Repositories;
using HardCodeOnlineShop.Domain.Entity;
using HardCodeOnlineShop.Service.Implementations;
using HardCodeOnlineShop.Service.Interfaces;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

builder.Services.AddDbContext<ApplicationDbContext>(options =>
        options.UseSqlServer(connectionString, optionsBuilder => optionsBuilder.MigrationsAssembly("HardCode")));

builder.Services.InitializeRepositories();
builder.Services.InitializeServices();

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

