﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HardCodeOnlineShop.Domain.Entity;
using HardCodeOnlineShop.Domain.ViewModels;
using HardCodeOnlineShop.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HardCodeOnlineShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public async Task<ActionResult<List<Product>>> GetProducts()
        {
            var products = await _productService.GetProducts();
            return Ok(products);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> GetProduct(int id)
        {
            var product = await _productService.GetProduct(id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        [HttpPost]
        public async Task<ActionResult<Product>> CreateProduct(ProductViewModel model)
        {
            var product = await _productService.CreateProduct(model);
            return CreatedAtAction(nameof(GetProduct), new { id = product.Id }, product);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProduct(int id, ProductViewModel model)
        {
            var product = await _productService.UpdateProduct(id, model);

            if (product == null)
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            var result = await _productService.DeleteProduct(id);

            if (result)
            {
                return NoContent();
            }

            return NotFound();
        }

        [HttpPost("filtered")]
        public async Task<ActionResult<List<Product>>> GetFilteredProducts(List<FilterParam> filterParams)
        {
            try
            {
                var products = await _productService.GetProducts();

                if (filterParams != null && filterParams.Count > 0)
                {
                    products = ApplyFilters(products, filterParams);
                }

                return Ok(products);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }

        private List<Product> ApplyFilters(List<Product> products, List<FilterParam> filterParams)
        {
            foreach (var filter in filterParams)
            {
                products = products.Where(p => p.Fields != null && p.Fields.Any(f =>
                    f != null && f.CategoryFieldId == filter.CategoryFieldId && f.Value == filter.Value))
                    .ToList();
            }

            return products;
        }
    }
}

