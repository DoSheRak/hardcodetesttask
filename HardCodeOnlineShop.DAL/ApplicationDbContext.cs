﻿using System;
using System.Reflection.Metadata;
using HardCodeOnlineShop.Domain.Entity;
using Microsoft.EntityFrameworkCore;

namespace HardCodeOnlineShop.DAL
{
	public class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
			: base(options)
		{
			Database.EnsureCreated();
		}

        public DbSet<Category> Categories { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<CategoryField> CategoryFields { get; set; }

        public DbSet<ProductField> ProductFields { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}

